<?php

namespace Database\Seeders;

use App\Models\product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        product::create([
            'name'=>'product1',
            'category'=>'category1',
            'picture'=>'http://uupload.ir/files/i6yi_icon-256x256.png',
            'description'=>'test'
        ]);

        product::create([
            'name'=>'product2',
            'category'=>'category2',
            'picture'=>'http://uupload.ir/files/i6yi_icon-256x256.png',
            'description'=>'test'
        ]);

        product::create([
            'name'=>'product3',
            'category'=>'category3',
            'picture'=>'http://uupload.ir/files/i6yi_icon-256x256.png',
            'description'=>'test'
        ]);
    }
}
